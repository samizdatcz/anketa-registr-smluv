$(document).ready(function() {
  $.getJSON( "https://interaktivni.rozhlas.cz/data/anketa-registr-smluv/data/data.json", function(data) {

    $(data).each(function(i) {
      if (this.veta.length>0) {$("<a class='open-popup-link' href='#popup-" + i + "'><div class='respondent'></div></a>").appendTo("#anketa")};
      if (this.veta.length>0) {$(".respondent").last().append("<div class='cedulka'>Klikněte pro celou odpověď</strong></div>")};
      if (this.veta.length==0) {$("<div class='respondent neodpovedel'></div>").appendTo("#anketa")};
      $(".respondent").last().append("<img class='portret' src='" + this.foto + "'>");
      $(".respondent").last().append("<p class='jmeno'><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.strana + "</p>");
      if (this.veta.length>0) {$(".respondent").last().append("<p class='veta'>&bdquo;" + this.veta + "&ldquo;</p>")};
      if (this.veta.length==0) {$(".respondent").last().append("<p class='veta' style='color:red;'>Bez odpovědi</p>")};
      $(".respondent").last().append("<div class='white-popup mfp-hide' id='popup-" + i + "'><p><strong>" + this.jmeno + " " + this.prijmeni + "</strong>, " + this.strana + "</p><p><em>1. Jak Senátem navržené změny hodnotíte, případně jak o nich budete hlasovat?</em></p><p>" + this.odpoved1 + "</p><p><em>2. Senát také podpořil návrh, který má zrušit platné výjimky ze zveřejňování pro Parlament, prezidentskou kancelář, Ústavní soud, Nejvyšší kontrolní úřad, úřad ombudsmanky a úřad Národní rozpočtové rady. Mají tyto úřady či instituce podle Vás zveřejňovat smlouvy v registru, či nikoli? Pokud ano/ne, z jakého důvodu?</em></p><p>" + this.odpoved2 + "</p></div>");
    });

    $(".cedulka").hide();

    $(".respondent").hover(
      function() {
        $(this).addClass("vybrany");
        $(this).find(".cedulka").show();
      }, function() {
        $(this).removeClass("vybrany");
        $(this).find(".cedulka").hide();
      }
    );

    $('.open-popup-link').magnificPopup({
      type:'inline',
      midClick: true // allow opening popup on middle mouse click. Always set it to true if you don't provide alternative source.
    });

  });
});