title: "„Když všichni, tak všichni.“ Jak se poslanci postaví k senátní verzi registru smluv?"
perex: ""
authors: ["Hana Mazancová", "Jan Boček"]
coverimg: https://interaktivni.rozhlas.cz/brexit/media/cover.jpg
coverimg_note: "Foto <a href='#'>ČTK</a>"
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1"]
---
V polovině dubna schválil Senát zrušení plošných výjimek ze zveřejňování v registru smluv. Postavil se tak proti Poslaneckou sněmovnou schválené novele, která výjimku udávala pro státní, krajské či obecní firmy. Poslanci mají nyní novelu zákona projednat znovu. A jak se k ní postaví? „Zveřejňovat smlouvy strategického významu by mohl požadovat jenom hlupák,“ říká například Jiří Koskuba z ČSSD.

Podle Horní komory Parlamentu by úlevu ze zveřejňování měl mít pouze národní pivovar Budějovický Budvar a zdravotnická zařízení. Senát zároveň podpořil návrh Michala Canova (SLK), který má zrušit platné výjimky ze zveřejňování smluv pro Parlament, Kancelář prezidenta republiky, Ústavní soud, Úřad veřejné ochránkyně práv, Nejvyšší kontrolní úřad a Národní rozpočtovou radu.

„Pokud máme mít registr smluv, tak bez výjimek,“ odpověděla například poslankyně Jitka Chalánková (TOP 09). Server iROZHLAS.cz uspořádal mezi poslanci a poslankyněmi anketu s dvěma dotazy:

<li>Jak Senátem navržené změny hodnotíte, případně jak o nich budete hlasovat?</li>
<li>Senát také podpořil návrh, který má zrušit platné výjimky ze zveřejňování pro Parlament, prezidentskou kancelář, Ústavní soud, Nejvyšší kontrolní úřad, úřad ombudsmanky a úřad Národní rozpočtové rady. Mají tyto úřady či instituce podle Vás zveřejňovat smlouvy v registru, či nikoli? Pokud ano/ne, z jakého důvodu?</li>

Podporu Senátem schválenému návrhu vyjádřil i poslanec Ivan Gabal (KDU-ČSL). „Senát potvrdil svou zásadní váhu v ústavním systému a v korekci sněmovny uvnitř Parlamentu,“ napsal. Proti výjimkám z registru se naopak v anketě vyslovil například poslanec Martin Komárek (ANO) nebo Antonín Seďa (ČSSD).

Poslanec Jan Klán (KSČM) napsal, že je „zásadně proti“, aby smlouvy zveřejňoval Parlament, kancelář prezidenta či Ústavní soud. „Je to ohrožení bezpečnosti státu jako celku! Ostatně o to soukromému kapitálu a neziskovým organizacím jde. Jde jim o to, aby viděli do všech smluv, které uzavírá stát a tím pádem jej mohli postupně oslabovat a poté nahradit. Je to cílený útok na fungování státu jako celku,“ napsal. Výjimku pro tyto instituce by chtěl i Augustin Karel Andrle Sylor (Úsvit).

Všechny došlé odpovědi si můžete přečíst níže. Poslanci, kteří v anketě nejsou, na naše otázky neodpověděli.

<div id="anketa">
   <h2 style="margin-left: 25px; margin-right: 25px; margin-bottom: 0px; font-size: 1.4em">Poslanecká anketa k registru smluv</h2>
   <h3 style="margin-left: 25px; margin-right: 25px; margin-bottom: 20px; margin-top: 30px; color: #606060">Kliknutím na respondenta zobrazíte jeho celé odpovědi</h3>
</div>